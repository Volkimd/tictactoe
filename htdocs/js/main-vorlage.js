// Die Mainfunktion kann direkt ins $ übergeben werden, als anonyme Funktion,
// also eine Funktion ohne Namen
$(function() {
    // "use strict" brauchen wir nur 1 mal hier drin zu schreiben.
    // Damit sagen wir, dass alles in dieser Funktion (in diesem 'closure') 
    // von der Javascriptengine strikter behandelt werden soll
    "use strict";

    // Diese innere Funktion ist ein Modul. 
    // - Variablen die hier deklariert werden sind nach aussen nicht sichtbar. 
    // - Aber jede Funktion die hier drin definiert wird, hat Zugriff auf diese Variablen.

    // Wir können jetzt anfangen unsere eigenen "globalen" Variablen zu deklarieren.
    // z.B. den counter

    let counter = 0;   
    let $h1 = $("h1");

    function updatePlayerInfo() {
        if (counter % 2) {
            $h1.text("Player 2");
        } else {
            $h1.text("Player 1");
        }
    }

    function onButtonClicked($btn) {
        console.log("button", $btn.attr("id"), "wurde geklickt");
        // hier greifen wir auf die Modulvariable zu
        counter++;
        // und aktualisieren den Text
        updatePlayerInfo();
    }

    $("button").on("click", function() {
        // den Button übergeben wir direkt als jQuery Objekt an
        // unseren Handler
        onButtonClicked($(this));
    });
});
