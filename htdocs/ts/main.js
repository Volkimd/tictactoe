/*jslint browser: true*/
/*eslint no-var: 'error'*/
/*eslint-env es6*/
/*global $, alert*/
$(function () {
    'use strict';
    var counter = 1, // Round Counter
    fullState = {}, $h1 = $('h1#gameMode'), // Header with Game Info
    player = '', checkBox, gameMode = '';
    // set Player x/o and change HEader
    function playerX(cell) {
        player = 'x';
        $('h1#state').text('Player: ' + player);
        checkBox = $(cell).attr('id');
    }
    function playerO(cell) {
        player = 'o';
        $('h1#state').text('Player: ' + player);
        checkBox = $(cell).attr('id');
    }
    function playerKI() {
        player = 'o';
        if (!fullState[5]) {
            checkBox = 5;
        }
        if (!fullState[6]) {
            checkBox = 6;
        }
    }
    // Fill Object with clickt sign and position TODO: + gamesequence(counter)
    function fillObject() {
        fullState[checkBox] = player;
    }
    function renderMap() {
        for (var key in fullState) {
            switch (key) {
                case '1':
                    $('.gameBTN#1').text(fullState[key]);
                    break;
                case '2':
                    $('.gameBTN#2').text(fullState[key]);
                    break;
                case '3':
                    $('.gameBTN#3').text(fullState[key]);
                    break;
                case '4':
                    $('.gameBTN#4').text(fullState[key]);
                    break;
                case '5':
                    $('.gameBTN#5').text(fullState[key]);
                    break;
                case '6':
                    $('.gameBTN#6').text(fullState[key]);
                    break;
                case '7':
                    $('.gameBTN#7').text(fullState[key]);
                    break;
                case '8':
                    $('.gameBTN#8').text(fullState[key]);
                    break;
                case '9':
                    $('.gameBTN#9').text(fullState[key]);
                    break;
            }
        }
    }
    // find winner by comparing gamestates
    function chooseWinner() {
        if ((fullState[1] === 'x' && fullState[2] === 'x' && fullState[3] === 'x') ||
            (fullState[4] === 'x' && fullState[5] === 'x' && fullState[6] === 'x') ||
            (fullState[7] === 'x' && fullState[8] === 'x' && fullState[9] === 'x') ||
            (fullState[1] === 'x' && fullState[4] === 'x' && fullState[7] === 'x') ||
            (fullState[2] === 'x' && fullState[5] === 'x' && fullState[8] === 'x') ||
            (fullState[3] === 'x' && fullState[6] === 'x' && fullState[9] === 'x') ||
            (fullState[3] === 'x' && fullState[5] === 'x' && fullState[7] === 'x') ||
            (fullState[1] === 'x' && fullState[5] === 'x' && fullState[9] === 'x')) {
            $('h1#state').text('Player X is The Winner');
            $('button.gameBTN').attr('disabled', true);
        }
        if ((fullState[1] === 'o' && fullState[2] === 'o' && fullState[3] === 'o') ||
            (fullState[4] === 'o' && fullState[5] === 'o' && fullState[6] === 'o') ||
            (fullState[7] === 'o' && fullState[8] === 'o' && fullState[9] === 'o') ||
            (fullState[1] === 'o' && fullState[4] === 'o' && fullState[7] === 'o') ||
            (fullState[2] === 'o' && fullState[5] === 'o' && fullState[8] === 'o') ||
            (fullState[3] === 'o' && fullState[6] === 'o' && fullState[9] === 'o') ||
            (fullState[1] === 'o' && fullState[5] === 'o' && fullState[9] === 'o') ||
            (fullState[3] === 'o' && fullState[5] === 'o' && fullState[7] === 'o') ||
            (fullState[1] === 'o' && fullState[5] === 'o' && fullState[9] === 'o')) {
            $('h1#state').text('Player O is The Winner');
            $('button.gameBTN').attr('disabled', true);
        }
        if (counter === 9) {
            $('h1#state').text('Draw');
        }
    }
    /* --------------------------------------------------------------*/
    /*Buttonclick which calls funktion playerChange for Player selcection,
  fillObject to get game state in an Dictionary
  and chooseWinner to find winner
  !!! This Function is called by switch in chooseGamemode*/
    /* Mode One VS One */
    function gameModeOneVsOne(cell) {
        if (counter % 2) {
            playerX(cell);
        }
        else {
            playerO(cell);
        }
        $(cell).attr('disabled', true);
        fillObject();
        renderMap(fullState);
        chooseWinner(fullState);
        counter += 1;
    }
    function gameModeOneVsKI(cell) {
        playerX(cell);
        fillObject(cell);
        playerKI();
        fillObject(cell);
        renderMap(fullState);
        chooseWinner(fullState);
        //console.log('VsKI');
    }
    function gameModeOneVsLAN() {
        counter += 1;
        //    console.log('VsLAN');
    }
    // Choose Mode and call Function chooseGamemode where gameBTNOnClick is homed
    // Selector for Gamemode LAN 1vs1 1vsPC
    function chooseGamemode(mode) {
        gameMode = ($(mode).attr('id'));
        switch (gameMode) {
            case '1vs1':
                $h1.text('One vs One');
                $('button.gameBTN').attr('disabled', false);
                break;
            case '1vsKI':
                $h1.text('User vs Computer');
                $('button.gameBTN').attr('disabled', false);
                break;
            case 'lan':
                $h1.text('One vs One over LAN');
                $('button.gameBTN').attr('disabled', false);
                break;
        }
    }
    //Hier gehts los
    $('button.gameBTN').attr('disabled', true);
    $('button.gameModeBTN#lan').attr('disabled', true);
    //-----------------------------------------------------//
    $('button.gameModeBTN').on('click', function () {
        chooseGamemode(this);
        for (var key in fullState) {
            delete fullState[key];
            $('button.gameBTN').text('');
        }
        counter = 1;
    });
    $('button.gameBTN').on('click', function () {
        switch (gameMode) {
            case '1vs1':
                gameModeOneVsOne(this);
                break;
            case '1vsKI':
                gameModeOneVsKI(this);
                break;
            case 'lan':
                gameModeOneVsLAN(this);
                break;
        }
    });
});
